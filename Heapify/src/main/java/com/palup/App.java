package com.palup;

import com.palup.drawing.TreeCanvas;

import javax.swing.*;

import static javax.swing.JFrame.EXIT_ON_CLOSE;

/**
 *
 */
public class App 
{
    public static void main(String[] args) throws InterruptedException {
        JFrame window = new JFrame();
        window.setSize(1000, 1000);
        window.setTitle("Heap Tree");
        window.setDefaultCloseOperation(EXIT_ON_CLOSE);

        TreeCanvas canvas1 = new TreeCanvas(0, 0);
        window.add(canvas1);
        window.setVisible(true);

        canvas1.insert(1);
        canvas1.insert(2);
        canvas1.insert(3);
        canvas1.insert(4);
        canvas1.insert(5);
        canvas1.insert(6);
        canvas1.insert(7);
        canvas1.insert(8);
        canvas1.insert(9);
        canvas1.insert(10);
        canvas1.insert(11);
        canvas1.insert(12);
        canvas1.insert(13);
        canvas1.insert(14);
        canvas1.insert(153);
        canvas1.insert(2);
    }
}
