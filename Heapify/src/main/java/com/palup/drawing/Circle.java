package com.palup.drawing;

import java.awt.geom.Ellipse2D;

/**
 * The <code>Circle</code> class describes a circle defined by
 * coordinates of center and radius.
 *
 * Created by kuldeep on 24/04/16.
 */
public class Circle extends Ellipse2D.Float {

    public Circle(int centerX, int centerY, int radius){
        super(centerX - radius, centerY - radius, 2 * radius, 2 * radius);
    }

    public Circle(int centerX, int centerY) {
        this(centerX, centerY, 20);
    }

}
