package com.palup.drawing;

import java.awt.geom.Line2D;

/**
 * The <code>LineCircleToCircle</code> is the shortest line
 * from circumference of one circle to another
 *
 * Created by kuldeep on 24/04/16.
 */
public class LineCircleToCircle extends Line2D.Float {

    public LineCircleToCircle(int startX, int startY, int endX, int endY) {
        super(startX, startY, endX, endY);
    }

    public LineCircleToCircle(float startX, float startY, float endX, float endY) {
        super(startX, startY, endX, endY);
    }

    /**
     * @param firstCircleX x coordinate of first circle
     * @param firstCircleY y coordinate of first circle
     * @param secondCircleX x coordinate of second circle
     * @param secondCircleY y coordinate of second circle
     * @param firstCircleRadius radius of first circle
     * @param secondCircleRadius radius of second circle
     *
     * @return instance of <code>LineCircleToCircle</code>
     */
    public static LineCircleToCircle getLine(int firstCircleX, int firstCircleY, int secondCircleX,
                                             int secondCircleY, int firstCircleRadius, int secondCircleRadius){
        float deltaX = secondCircleX - firstCircleX;
        float deltaY = secondCircleY - firstCircleY;
        float distanceBetweenCenters = (float) Math.sqrt(deltaX * deltaX + deltaY * deltaY);

        float startX = firstCircleX + firstCircleRadius * (secondCircleX - firstCircleX)/distanceBetweenCenters;
        float startY = firstCircleY + firstCircleRadius * (secondCircleY - firstCircleY)/distanceBetweenCenters;

        float endX = secondCircleX - secondCircleRadius * (secondCircleX - firstCircleX)/distanceBetweenCenters;
        float endY = secondCircleY - secondCircleRadius * (secondCircleY - firstCircleY)/distanceBetweenCenters;

        return new LineCircleToCircle(startX, startY, endX, endY);
    }

    /**
     * @param firstCircleX x coordinate of first circle
     * @param firstCircleY y coordinate of first circle
     * @param secondCircleX x coordinate of second circle
     * @param secondCircleY y coordinate of second circle
     * @param radius radius of both circles
     *
     * @return instance of <code>LineCircleToCircle</code>
     */
    public static LineCircleToCircle getLine(int firstCircleX, int firstCircleY, int secondCircleX,
                                             int secondCircleY, int radius){

        return getLine(firstCircleX, firstCircleY, secondCircleX, secondCircleY, radius, radius);
    }
}
