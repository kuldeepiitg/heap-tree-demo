package com.palup.drawing;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Tree canvas, whole heap is rendered on it.
 *
 * Created by kuldeep on 24/04/16.
 */
public class TreeCanvas extends JPanel {

    /**
     * Array containing a heap.
     */
    private ArrayList<Integer> array;

    /**
     * X,Y coordinates of top left corner.
     */
    private int x, y;

    /**
     * Proper radius of nodes
     */
    private int radius = 20;

    /**
     * Vertical distance between two consecutive levels.
     */
    private int levelDistance = 160;

    /**
     * Distance between centers of two consecutive nodes at same level.
     * It is also equal to net influential diameter of node, that is proper
     * diameter plus empty space.
     * <p>
     * 20                   40                 20
     * |------------>|------------------------|<------------|
     * C1         Radius                   Radius           C2
     * center     intersecting             intersecting     center
     * of first   circumference            circumference    of second
     * node       of first node            of second node   node
     */
    private int nodeDistance = 80;

    /**
     * Height of character
     */
    private int charHeight = 6;

    /**
     * Width of character
     */
    private int charWidth = 8;

    /**
     * Pause time
     */
    private long pauseTime = 2 * 1000; // 2 seconds

    private ArrayList<Integer> previousState;

    public TreeCanvas(int topLeftX, int topLeftY) {
        this.y = topLeftY;
        this.x = topLeftX;
        this.array = new ArrayList<Integer>();
        this.previousState = new ArrayList<Integer>();
    }

    /**
     * Insert an element into heap, such that heap property is maintained
     *
     * @param element element to be inserted
     */
    public void insert(int element) throws InterruptedException {
        array.add(element);
        heapify();
    }

    @Override
    public void paint(Graphics graphics) {
        Graphics2D graphics2D = (Graphics2D) graphics;
        graphics2D.setStroke(new BasicStroke(2));
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        int levelCounts = (int) Math.ceil(Math.log10(array.size() + 1) / Math.log10(2));

        // middleX is x coordinate of vertical line passing through root node
        int middleX;
        if (levelCounts == 1) {
            middleX = 40;
        } else {
            int maxWidthRequiredAtAnyLevel = (int) Math.pow(2, levelCounts - 1) * nodeDistance;
            middleX = maxWidthRequiredAtAnyLevel / 2;
        }

        // Store coordinate of each node in same order as in the heap array
        ArrayList<Point> pointsCoordinate = new ArrayList<Point>();

        for (int i = 0; i < levelCounts; i++) {

            int maxNodeCountForCurrentLevel = (int) Math.pow(2, i);

            int initialLeftOffset;
            if (i == 0) {
                initialLeftOffset = middleX;
            } else {
                initialLeftOffset = middleX - maxNodeCountForCurrentLevel / 2 * nodeDistance + nodeDistance / 2;

            }
            int topOffset = 80 + i * levelDistance; // it is pointY
            for (int j = 0; j < maxNodeCountForCurrentLevel && maxNodeCountForCurrentLevel - 1 + j < array.size(); j++) {
                int pointX = initialLeftOffset + j * nodeDistance;
                pointsCoordinate.add(new Point(pointX, topOffset));

                Circle circle = new Circle(pointX, topOffset, radius);
                graphics2D.draw(circle);

                int index = (int) Math.pow(2, i) - 1 + j;

                if (index >= previousState.size()) {
                    graphics2D.setColor(Color.yellow);
                    graphics2D.fill(circle);
                    previousState.add(array.get(index));
                } else if (array.get(index) > previousState.get(index)) {
                    graphics2D.setColor(Color.GREEN);
                    graphics2D.fill(circle);
                    previousState.set(index, array.get(index));
                } else if (array.get(index) < previousState.get(index)) {
                    graphics2D.setColor(Color.RED);
                    graphics2D.fill(circle);
                    previousState.set(index, array.get(index));
                }
                graphics2D.setColor(Color.BLACK);

                String label = array.get(index).toString();
                int labelOffsetFromCenterX = (int) -(charWidth * (Math.log10(array.get(index)) + 1) / 2);
                int labelOffsetFromCenterY = charHeight / 2;
                graphics2D.drawString(label, pointX + labelOffsetFromCenterX, topOffset + labelOffsetFromCenterY);
            }
        }

        for (int i = 1; i < pointsCoordinate.size(); i++) {
            Point second = pointsCoordinate.get(i);
            Point first = pointsCoordinate.get((i-1)/2);
            LineCircleToCircle line = LineCircleToCircle.getLine(first.xCoordinate, first.yCoordinate, second.xCoordinate, second.yCoordinate, radius);
            graphics2D.draw(line);
        }
    }

    /**
     * Heapify the array
     *
     * @throws InterruptedException
     */
    private void heapify() throws InterruptedException {

        repaint();
        Thread.sleep(pauseTime);

        int index = array.size() - 1;
        while (index > 0 && array.get(index) > array.get((index - 1) / 2)) {
            int temp = array.get(index);
            array.set(index, array.get((index - 1) / 2));
            array.set((index - 1) / 2, temp);
            repaint();
            Thread.sleep(pauseTime);
            index = (index - 1) / 2;
        }
        repaint();
    }

    /**
     * The point on canvas
     */
    private class Point {

        /**
         * X-coordinate of the point
         */
        public int xCoordinate;

        /**
         * Y-coordinate of the point
         */
        public int yCoordinate;

        public Point(int x, int y){
            this.xCoordinate = x;
            this.yCoordinate = y;
        }
    }
}
